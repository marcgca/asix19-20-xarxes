

# xpath

http://xpather.com/

http://www.xpathtester.com/xpath

https://lxml.de/tutorial.html

https://docs.python.org/3.6/library/xml.etree.elementtree.html

definicion

tecnologia para hacer consultas en un archivo xml

en python en unicode busquedas u//

## Propiedades

```
.text
.tag
.attrib
.tail
```



## Búsquedas 

```bash
# /  ruta absoluta
/biblioteca/libro/titulo
# //    descendiente
//titulo
/biblioteca//titulo
# @ atributo
//autor/@fechaNacimiento
# [] where
//autor[@fechaNacimiento] autor que contenga atributo data nacimiento
//autor[@fechaNacimiento="28/03/1936"]
//autor[.='Milan Kundera']
# .. nodo padre
//autor[.='Milan Kundera']/..
//libro[autor='Milan Kundera'] equivalente
# posiciones
//libro[1]
//libro[last()]
//libro[last()-1]
//libro[position()<2]
# contenido 
//autor/text()
# contenido del nodo
//libro/node() muestra contenido de nodo y sus hijos
# contador de elementos
//carrera[count(subdirector) = 0]
# buscar en descendencia
//libro[.//autor]
```

```xml
<biblioteca>
  <libro>
    <titulo>La vida está en otra parte</titulo>
    <autor>Milan Kundera</autor>
    <fechaPublicacion año="1973"/>
  </libro>
  <libro>
    <titulo>Pantaleón y las visitadoras</titulo>
    <autor fechaNacimiento="28/03/1936">Mario Vargas Llosa</autor>
    <fechaPublicacion año="1973"/>
  </libro>
  <libro>
    <titulo>Conversación en la catedral</titulo>
    <autor fechaNacimiento="28/03/1936">Mario Vargas Llosa</autor>
    <fechaPublicacion año="1969"/>
  </libro>
</biblioteca>
```

## Python3

https://www.josedomingo.org/pledin/2015/01/trabajar-con-ficheros-xml-desde-python_1/

https://lxml.de/api/lxml.etree._Element-class.html

## Metodos

```
.parse	# recoger el arbol xml de un documento
.getroot	# recoge el elemento raiz del arbol
.tostring	# metodo de string
.dump		# metodo de string
.Element	# crear elemento
.SubElement	# crear elemento descendiente de
.append		# añadir elemento mhijo
.find		# buscar, devuelve la primera coincidencia
.findall	# buscar, devuelve todas las coincidencias
.findtext	# buscar, devuelve el texo del elemento coincidente
.set		# añadir atributo
.get		# visualizar atributo
.remove
.xpath		# busqueda de ruta
```



### String

Diferentes maneras de mostrar el contenido de un elemento, con `tostring o dump`  

```python
>>> from lxml import etree
# recoger arbol del documento
>>> arbol = etree.parse('libros.xml')
# recoger elemento raiz
>>> raiz = arbol.getroot()

>>> etree.tostring(raiz, pretty_print=True)
b'<biblioteca>\n  <libro>\n    <titulo>La vida est&#225; en otra parte</titulo>\n    <autor>Milan Kundera</autor>\n    <fechaPublicacion a&#241;o="1973"/>\n  </libro>\n  <libro>\n    <titulo>Pantale&#243;n y las visitadoras</titulo>\n    <autor fechaNacimiento="28/03/1936">Mario Vargas Llosa</autor>\n    <fechaPublicacion a&#241;o="1973"/>\n  </libro>\n  <libro>\n    <titulo>Conversaci&#243;n en la catedral</titulo>\n    <autor fechaNacimiento="28/03/1936">Mario Vargas Llosa</autor>\n    <fechaPublicacion a&#241;o="1969"/>\n  </libro>\n</biblioteca>\n'

>>> etree.dump(raiz)
<biblioteca>
  <libro>
    <titulo>La vida está en otra parte</titulo>
    <autor>Milan Kundera</autor>
    <fechaPublicacion año="1973"/>
  </libro>
  <libro>
    <titulo>Pantaleón y las visitadoras</titulo>
    <autor fechaNacimiento="28/03/1936">Mario Vargas Llosa</autor>
    <fechaPublicacion año="1973"/>
  </libro>
  <libro>
    <titulo>Conversación en la catedral</titulo>
    <autor fechaNacimiento="28/03/1936">Mario Vargas Llosa</autor>
    <fechaPublicacion año="1969"/>
  </libro>
</biblioteca>
>>> 
```



### Movimientos básicos



```python
>>> from lxml import etree
>>> doc = etree.parse('libros.xml')
>>> raiz=doc.getroot()
>>> raiz.tag
biblioteca

# mostrar primer elemento
>>> raiz[0].tag
libro

# mostrar contenido del primer elemento
>>> etree.dump(raiz[0])
<libro>
    <titulo>La vida está en otra parte</titulo>
    <autor>Milan Kundera</autor>
    <fechaPublicacion año="1973"/>
</libro>

# mostrar primer titulo del primer libro
>>> print raiz[0][0].tag
titulo

# mostrar contenido del primer titulo del primer libro
>>> print raiz[0][0].text
La vida está en otra parte

# mostrar contenido del segundo elemento del primer libro
>>> print raiz[0][1].text
Milan Kundera

# mostrar atributo ( caracteres extraños son por la letra ñ )
>>> print raiz[0][2].attrib[u'a\xf1o']
1973

# titulo y año del 3r libro
>>> print( raiz[2][2].attrib[u'a\xf1o'],  raiz[2][0].text )
1969 Conversación en la catedral
```



### xpath

Python junto a xpath recorre el árbol con una ruta de destino.

```python
>>> from lxml import etree
>>> arbol = etree.parse('libros.xml')
>>> autores = arbol.xpath(u"/biblioteca/libro/autor")
# devuelve una lista de objetos que coinciden con la busqueda.
>>> autores
[<Element autor at 0x7f7e8bb0bac8>, <Element autor at 0x7f7e8bb0ba88>, <Element autor at 0x7f7e8bb0bd08>]

>>> for autor in autores:
...     autor.text
... 
'Milan Kundera'
'Mario Vargas Llosa'
'Mario Vargas Llosa'
```



### Find

find retorna el primer elemento que encuentra partiendo tando del arbol del documento como de un elemento.

```python
>>> from lxml import etree
>>> arbol = etree.parse('libros.xml')
>>> raiz=arbol.getroot()

>>> titulo = arbol.find('//titulo')
>>> titulo
<Element titulo at 0x7f7e8bb0bdc8>
>>> titulo.tag
'titulo'
>>> titulo.text
'La vida está en otra parte'

>>> raiz.tag
'biblioteca'
>>> raiz.find("libro/titulo")
<Element titulo at 0x7f7e8bb0bdc8>

```



#### findall

findall retorna una lista de elementos

```python
>>> from lxml import etree
>>> arbol = etree.parse('libros.xml')
>>> raiz=arbol.getroot()

arbol.findall('//titulo')
[<Element titulo at 0x7f7e8bb0bdc8>, <Element titulo at 0x7f7e8bb0bec8>, <Element titulo at 0x7f7e8bb0bcc8>]

>>> raiz.findall("libro/titulo")
[<Element titulo at 0x7f7e8bb0bdc8>, <Element titulo at 0x7f7e8bb0bec8>, <Element titulo at 0x7f7e8bb0bcc8>]
```



#### findtext

findtext retorna el contenido del elemento a buscar

```python
>>> from lxml import etree
>>> arbol = etree.parse('libros.xml')
>>> raiz=arbol.getroot()

>>> raiz.findtext("libro/titulo")
'La vida está en otra parte'

>>> arbol.findtext('//titulo')
'La vida está en otra parte'
```



### Element, SubElement, append

Element permite crear elementos que después se puede añadir con append. La característica de append es que el argumento a pasar a de ser un objeto y hay que partir del elemento padre.

SubElement permite crear y añadir directamente un elemento hijo.

```python
>>> from lxml import etree
>>> arbol = etree.parse('libros.xml')
>>> raiz=arbol.getroot()
>>> 
>>> libro = raiz.find('./libro')

>>> genero = etree.Element('genero')
>>> libro.append(genero)
>>> etree.dump(libro)
<libro>
    <titulo>La vida está en otra parte</titulo>
    <autor>Milan Kundera</autor>
    <fechaPublicacion año="1973"/>
    <genero/>
</libro> 

>>> etree.SubElement(libro, 'editorial')
>>> etree.dump(libro)
<libro>
    <titulo>La vida está en otra parte</titulo>
    <autor>Milan Kundera</autor>
    <fechaPublicacion año="1973"/>
    <genero/>
    <editorial/>
</libro>
```



### Remove

Las características de remove son, que tienes que partir del elemento padre y como argumento tiene que ser un objeto.

```python
>>> etree.dump(libro)
<libro>
    <titulo>La vida está en otra parte</titulo>
    <autor>Milan Kundera</autor>
    <fechaPublicacion año="1973"/>
    <genero/>
    <editorial/>
</libro>

>>> editorial = libro.find('./editorial')

>>> libro.remove(editorial)

>>> etree.dump(libro)
<libro>
    <titulo>La vida está en otra parte</titulo>
    <autor>Milan Kundera</autor>
    <fechaPublicacion año="1973"/>
  <genero/>
</libro>
```





### Contenido



```python
>>> etree.dump(cd)
<cd>
  <a/>
  <b/>
  <c/>
</cd>
>>> cd[0].tag
'a'
>>> cd[0].text
>>> cd[0].text='añadiendo'
>>> cd[0].text
'añadiendo'
>>> cd[0].text='actualizando'
>>> cd[0].text
'actualizando'
>>> cd[0].text=''
>>> cd[0].text
''
>>> etree.dump(cd)
<cd>
  <a></a>
  <b/>
  <c/>
</cd>
```



#### atributos

```python
>>> etree.dump(cd)
<cd>
  <a></a>
  <b/>
  <c/>
</cd>

# añadir las 2 valen
>>> cd[1].attrib['id']='3'
>>> cd[0].set('id', '1')
>>> etree.dump(cd)
<cd>
  <a id="1"></a>
  <b id="3"></b>
  <c/>
</cd>

# mostrar
>>> cd[0].get('id')
'1'
>>> cd[0].attrib
{'id': '1'}
>>> cd[0].attrib['id']
'1'

# actualizar
>>> cd[0].attrib['id']='2'
>>> cd[0].attrib['id']
'2'

# eliminar
>>> del(cd[0].attrib['id'])
>>> etree.dump(cd)
<cd>
  <a></a>
  <b id="3"></b>
  <c/>
</cd>
```



## Formatear

maneras de formatear el archivo externo.

```bash
echo "$(xmllint catalog2.xml --format)" > catalog2.xml 
xmllint catalog2.xml --format | sponge catalog2.xml
```