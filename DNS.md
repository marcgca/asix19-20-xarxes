# DNS

DNS és un protocol de resolució de noms (Domain Name Service) que és binari, és a dir, client-servidor. El client demana una resolució de nom i el servidor li dóna.

DNS utilitza el paquet `bind` que conté el servei `named`, amb el qual es pot interactuar amb la comanda `rndc`.

* Algunes opcions de `named`:
  
  ```bash
  named -c config-file # fa servir el fitxer de configuració especificat
  named -d debug-level [0-15]
  named -f -g # logs en foreground
  named -L /dev/stderr # indica un logfile en comptes del system log
  named -u (user) # Per defecte és named
  ```

## Configuració

Podem trobar la configuració a `/etc/named/` i a `/var/named/` 

* A `/etc/named` trobem la configuració i l'arxiu principal de zones:
  
  ```bash
  [root@localhost ~]# ll /etc/named
  named/               named.conf           named.rfc1912.zones  named.root.key
  ```

* A `/var/named` trobem la configuració individual per a cada zona:
  
  ```bash
  [root@localhost ~]# ll /var/named/
  total 28
  drwxrwx---. 2 named named 4096 ago 28 20:18 data
  drwxrwx---. 2 named named 4096 ago 28 20:18 dynamic
  -rw-r-----. 1 root  named 2253 feb 15  2019 named.ca
  -rw-r-----. 1 root  named  152 dic 15  2009 named.empty
  -rw-r-----. 1 root  named  152 jun 21  2007 named.localhost
  -rw-r-----. 1 root  named  168 dic 15  2009 named.loopback
  drwxrwx---. 2 named named 4096 ago 28 20:18 slaves
  ```

* A `/etc/hosts` tenim els hosts que coneix la nostra màquina. És a dir, que si ens referrim a algún d'aquests, no ho haurà de demanar al DNS.
  
  ```bash
  [root@localhost ~]# cat /etc/hosts
  127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
  ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
  ```

## Utilitats per a resolucions de dns

Amb el paquet `bind-utils` ens venen eines per a la resolució de noms (sol instalar-se amb bind).

`nslookup` i `host` per defecte **no** miren el fitxer `/etc/hosts`.

```bash
[marc@localhost ~]$ dig yahoo.es +short
212.82.100.151
74.6.136.151
98.136.103.24
124.108.115.101
106.10.248.151
# Amb -x fem la resolució inversa, també li podem especificar a quina màquina fer la cerca amb @server
[marc@localhost ~]$ dig -x 8.8.8.8 +short
dns.google.
```

## Documentació oficial

Al instal·lar el paquet `bind` també descarrega tota la seva documentació:

```bash
# doc original
/usr/share/doc/bind/Bv9ARM.html
/usr/share/doc/bind/Bv9ARM.pdf
# exemples
/usr/share/doc/bind/shample

```

## Estadístiques

Es poden generar estadístiques del trafic del DNS, sempre que tinguem la següent opció a `/etc/named.conf`:

```bash
...
    statistics-file "/var/named/data/named_stats.txt";
...

```

Si la tenim:

```bash
# Activem el servei
systemctl start named
# Activem les estadístiques
rndc stats
```

## ACLs

Podem definir un alias a una o a varies ips:

```bash
acl aula {10.200.243.0/24;};
options {
    listen-on port 53 { 127.0.0.1; 10.200.243.209; };
    directory     "/var/named";
    dump-file     "/var/named/data/cache_dump.db";
    statistics-file "/var/named/data/named_stats.txt";
    memstatistics-file "/var/named/data/named_mem_stats.txt";
    allow-query     { localhost; aula; };
```

## Reenviament

Podem reenviar una petició a una altre màquina al fitxer `/etc/named.conf` a l'apartat *options*:

* Podem escollir que primer intenti resoldre amb les seves zones, si no que ho reenvi amb `first`.

* Amb `only` ho reenviem directament:

```bash
forward only;
forwarders {10.1.1.200;};    
recursion yes;
```

## Esborrar chaché

Podem esborrar la caché de la nostra màquina amb `rndc flush`.

## Zones

Un fitxer de zona és una llista de *resource-records*, és a dir, una llista de resolucions domini-ip (**directa**) i ip-domini(**inversa**).

Al fitxer `/etc/named.conf` s'especifiquen i a quin fitxer de zona fa referència (aquí tindrem la configuració de la zona).

```bash
# directa
zone "lacasitos.com" IN {
	type master;
	file "lacasitos.com.zone";
}
# inversa
zone "243.200.10.in-addr.arpa" IN {
	type master;
	file "lacasitos.com.rev.zone";
}
```

**Important**: Com podem comprovar, la zona directa es defineix com el domini principal (**lacasitos.com**), mentres que la zona inversa es defineix amb la ip del servidor del revés fins l'octet que canvia, seguit de `in-addr.arpa`.

Els fitxers de zona si es defineixen amb una ruta parcial s'han de trobar al directori establert a l'opció `directory "/var/named";`.

Les línies dels fitxers de zona es defineixen pels elements `name, ttl, record class, record type, record data`. Si algún no s'especifica tindrà el valor global definit al inici de l'arxiu o el de la línia anterior.

* `name`

* `TTL`: Temps de vida

* `record class`: Classe de registre

* `record type`: Tipus de registre (`SOA, A, AAAA, CNAME, PTR, MX...`)

* `record data`: Les dades del registre, podràn consistir d'un o més element depenent del tipus de registre.

Com a mínim un fitxer de zona ha d'especificar l'inici d'autoritat (**SOA**) amb el nom del servidor mestre, l'adreça de correu de la persona responsable i la llista d'arguments de temps de caducitat:

```bash
# origin	class	type	server-fqdn	 			mail					arguments
@			IN		SOA		jorge.lacasitos.com.	mailhost.lacasitos.com. ( 0 1D 1H 1W	3H )

```

## Exemples de fitxers de zona

Especificarem una zona anomenada "lacasitos":

*lacasitos.com.zone*

```bash
; file  lacasitos.com
$ORIGIN lacasitos.com.
$TTL 1D
@	IN	SOA	jorge.lacasitos.com.	mailhost.lacasitos.com. (
					0	; serial
					1D	; refresh
					1H	; retry
					1W	; expire
					3H )	; minimum

; DNS NS Records
@		IN		NS		jorge.lacasitos.com.

; DNS A records
marc	IN		A		10.200.243.8
jorge	IN		A		10.200.243.9
pau		IN		A		10.200.243.10
supertux	IN	CNAME	jorge

```

* El `.` indica que es un FQDN. Amb això indiquem que és un nom complet, si no s'hi afegiria el domini.
  
  * `@ IN NS jorge.lacasitos.com.` = `@ IN NS jorge`
  
  * `@ IN NS jorge.lacasitos.com` = `@ IN NS jorge.lacasitos.com.lacasitos.com.`

* Si no es defineix l'atribut `$ORIGIN` tindrà el valor del nom de la zona especificat a **named.conf**.

* La `@` es substitueix pel valor de `$ORIGIN`.

El fitxer de zona inversa els registres *SOA* i *NS* seràn els mateixos que el fitxer de zona normal, però en comptes dels registres `A` seràn els registres `PTR`.

*lacasitos.com.rev.zone*

```bash
; file zone 243.200.10
$TTL 1D
@	IN SOA	jorge.lacasitos.com. mailhost.lacasitos.com. (
					0	; serial
					1D	; refresh
					1H	; retry
					1W	; expire
					3H )	; minimum

; DNS NS records
@	NS	jorge.lacasitos.com.

; DNS PTR records
8	IN	PTR		marc.lacasitos.com.
9	IN	PTR		jorge.lacasitos.com.
10	IN	PTR		pau.lacasitos.com.

```

* Com podem comprovar començem per **l'últim octet de la ip** i acabem amb el **FQDN**.
